﻿using System;
using Microsoft.AspNetCore.Http;

namespace DatingAppTuto.Dtos
{
    public class PhotoForCreatingDto
    {
        public string Url {get; set; }
        public IFormFile File { get; set; }
        public string Description { get; set; }
        public DateTime DateAdded { get; set; }
        public string PublicID { get; set; }

        public PhotoForCreatingDto()
        {
            DateAdded = DateTime.Now;
        }
    }
}