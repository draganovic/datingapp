﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DatingAppTuto.Dtos
{
    public class UserForRegisterDto
    {
        [Required]
        [StringLength(15, MinimumLength = 4 , ErrorMessage = "You must specify a username between 4 and 15 caracters")]
        public string Username { get; set; }
        [Required]
        [StringLength(30,MinimumLength = 8 , ErrorMessage = "You must specify a password between 8 and 30 caracters")]
        public string Password { get; set; }
        [Required]
        public string Gender { get; set; }
        [Required]
        public string KnownAs { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Country { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastActive { get; set; }

        public UserForRegisterDto()
        {
            Created = DateTime.Now;
            LastActive = DateTime.Now;
        }
    }
}