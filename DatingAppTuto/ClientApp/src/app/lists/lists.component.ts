import { Component, OnInit } from '@angular/core';
import {User} from "../_models/user";
import {PaginatedResult, Pagination} from "../_models/Pagination";
import {AlertifyService} from "../_services/alertify.service";
import {UserService} from "../_services/user.service";
import {AuthService} from "../_services/auth.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css']
})
export class ListsComponent implements OnInit {
  users: User[];
  pagination: Pagination;
  likesParam: string;

  constructor(private alertify: AlertifyService,
              private route: ActivatedRoute,
              private userService: UserService) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.users = data["users"].result;
      this.pagination = data["users"].pagination;
    });
    this.likesParam = "likers";
  }

  loadUsers(){
    this.userService.getUsers(this.pagination.currentPage, this.pagination.itemsPerPage, null, this.likesParam)
      .subscribe((res: PaginatedResult<User[]>) => {
        this.users = res.result;
        this.pagination = res.pagination;
      },error1 => {
        this.alertify.error(error1);
      });
  }

}
