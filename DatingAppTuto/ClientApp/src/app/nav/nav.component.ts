import { Component, OnInit } from '@angular/core';
import {AuthService} from "../_services/auth.service";
import {AlertifyService} from "../_services/alertify.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  model: any = {};
  photoUrl: string;
  constructor( private authService: AuthService, private alertify: AlertifyService, private router: Router) { }

  ngOnInit() {
    this.authService.currentPhotoUrl.subscribe(photoUrl => this.photoUrl = photoUrl)
  }

  login(){
    this.authService.login(this.model).subscribe(Data => {
      this.alertify.success("Logged in successfully.");
    },error => {
      this.alertify.error("Failed to login.");
    },() => {
      this.router.navigate(['app/members']);
    });
  }
  loggedIn(){
    return this.authService.loggedIn();
  }
  logout(){
    this.authService.userToken = null;
    this.authService.currentUser = null;
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    this.alertify.message("Logged Out");
    this.router.navigate(['/home']);
  }
}
