﻿import {User} from "./user";

export class LoginModel {
  tokenString: string;
  user: User;
}
