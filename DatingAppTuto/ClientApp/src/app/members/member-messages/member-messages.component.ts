import {Component, Input, OnInit} from '@angular/core';
import {Message} from "../../_models/message";
import {AuthService} from "../../_services/auth.service";
import {UserService} from "../../_services/user.service";
import {AlertifyService} from "../../_services/alertify.service";
import * as _ from "underscore";
import {tap} from "rxjs/operators";

@Component({
  selector: 'app-member-messages',
  templateUrl: './member-messages.component.html',
  styleUrls: ['./member-messages.component.css']
})
export class MemberMessagesComponent implements OnInit {
  @Input() userId: number;
  messages: Message[];
  newMessage: any = {};

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private alertify: AlertifyService
  ) {
  }

  ngOnInit() {
    this.loadMessages();
  }

  loadMessages() {
    const currentUserId = +this.authService.decodedToken.nameid;
    this.userService.getMessageThread(this.authService.decodedToken.nameid, this.userId)
      .pipe(tap(messages => {
        _.each(messages, (message: Message) => {
          if (message.isRead === false && message.recipientId === currentUserId){
            this.userService.markAsRead(currentUserId, message.id)
          }
        })
      }))
      .subscribe(messages => {
        this.messages = messages
      }, error1 => {
        this.alertify.error(error1);
      });
  }

  sendMessage(){
    this.newMessage.recipientId = this.userId;
    this.userService.sendMessage(this.authService.decodedToken.nameid, this.newMessage).subscribe(message => {
      this.messages.unshift(message);
      this.newMessage.content = "";
    }, error1 => {
      this.alertify.error(error1);
    })
  }
}
