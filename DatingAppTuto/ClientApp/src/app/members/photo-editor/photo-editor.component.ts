import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {Photo} from "../../_models/photo";
import {FileUploader} from "ng2-file-upload";
import {AuthService} from "../../_services/auth.service";
import {UserService} from "../../_services/user.service";
import {AlertifyService} from "../../_services/alertify.service";
import * as _ from "underscore";

@Component({
  selector: 'app-photo-editor',
  templateUrl: './photo-editor.component.html',
  styleUrls: ['./photo-editor.component.css']
})
export class PhotoEditorComponent implements OnInit {

  @Input() photos: Photo[];
  @Output() getMemberPhotoChange = new EventEmitter<String>();
  uploader: FileUploader;
  hasBaseDropZoneOver = false;
  curerntMain: Photo;
  private readonly baseUrl: string;

  constructor(@Inject('BASE_URL') baseUrl: string,
              private authServie: AuthService,
              private userService: UserService,
              private alertify: AlertifyService) {
    this.baseUrl = baseUrl;
  }

  ngOnInit() {
    this.initializeUploader();
  }

  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }

  initializeUploader(){
    this.uploader = new FileUploader({
      url: this.baseUrl + "api/users/" + this.authServie.decodedToken.nameid + "/photos",
      authToken: "Bearer " + localStorage.getItem("token"),
      isHTML5: true,
      allowedFileType: ['image'],
      removeAfterUpload: true,
      autoUpload: false,
      maxFileSize: 10 * 1024 * 1024,
    });

    this.uploader.onSuccessItem = (item, response, status, headers) => {
      if (response) {
        const res: Photo = JSON.parse(response);
        const photo = {
          id: res.id,
          url: res.url,
          dateAdded: res.dateAdded,
          description: res.description,
          isMain: res.isMain
        };
        this.photos.push(photo);
        if (photo.isMain) {
          this.authServie.changeMemberPhoto(photo.url);
          this.authServie.currentUser.photoUrl = photo.url;
          localStorage.setItem("user", JSON.stringify(this.authServie.currentUser));
        }
      }
    }
  }

  setMainPhoto(photo: Photo){
    this.userService.setMainPhoto(this.authServie.decodedToken.nameid, photo.id).subscribe(() => {
      this.curerntMain = _.findWhere(this.photos, {isMain: true});
      this.curerntMain.isMain = false;
      photo.isMain = true;
      this.authServie.changeMemberPhoto(photo.url);
      this.authServie.currentUser.photoUrl = photo.url;
      localStorage.setItem("user", JSON.stringify(this.authServie.currentUser));
    }, error1 => {
      this.alertify.error(error1);
    });
  }

  deletePhoto(photoId: number){
    this.alertify.confirm("Are you sure you want to delete this photo ?", () => {
      this.userService.deletePhoto(this.authServie.decodedToken.nameid, photoId).subscribe(() => {
        this.photos.splice(_.findIndex(this.photos, {id : photoId}),1);
        this.alertify.success("Photo has been deleted");
      },error1 => {
        this.alertify.error("failed to delete photo.");
      } );
    });
  }

}
