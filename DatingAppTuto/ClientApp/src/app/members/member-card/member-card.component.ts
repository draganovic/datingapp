import {Component, Input, OnInit} from '@angular/core';
import {User} from "../../_models/user";
import {UserService} from "../../_services/user.service";
import {AuthService} from "../../_services/auth.service";
import {AlertifyService} from "../../_services/alertify.service";


@Component({
  selector: 'app-member-card',
  templateUrl: './member-card.component.html',
  styleUrls: ['./member-card.component.scss']
})
export class MemberCardComponent implements OnInit {
  @Input() user: User;

  constructor(private alertify: AlertifyService,
              private userService: UserService,
              private authService: AuthService) { }

  ngOnInit() {
  }

  sendLike(id: number){
    this.userService.sendLike(this.authService.decodedToken.nameid,id).subscribe(DataView => {
      this.alertify.success("You have liked: " + this.user.knownAs);
    }, error => {
      this.alertify.error(error);
    });
  }

}
