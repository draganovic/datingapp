import {throwError as observableThrowError, Observable} from 'rxjs';
import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpResponse, HttpHeaders} from "@angular/common/http";
import {User} from "../_models/user";
import "rxjs/add/operator/map"
import {PaginatedResult} from "../_models/Pagination";
import {map, tap} from "rxjs/operators";
import {Message} from "../_models/message";

@Injectable()
export class UserService {
  private readonly baseUrl: string;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  getUsers(page?: number, itemsPerPage?: number, userParams?: any, likesParam?: string): Observable<PaginatedResult<User[]>> {
    const paginatedResult: PaginatedResult<User[]> = new PaginatedResult<User[]>();
    let queryString = "?";
    if (page != null && itemsPerPage != null) {
      queryString += "pageNumber=" + page + "&pageSize=" + itemsPerPage + "&";
    }

    if (likesParam === "likers") {
      queryString += "likers=true&"
    }

    if (userParams != null) {
      queryString +=
        "minAge=" + userParams.minAge +
        "&maxAge=" + userParams.maxAge +
        "&gender=" + userParams.gender +
        "&orderBy=" + userParams.orderBy;
    }
    return this.http.get(this.baseUrl + "api/users" + queryString, {observe: 'response'})
      .pipe(map((res: HttpResponse<any>) => {
        paginatedResult.result = res.body;
        if (res.headers.get("Pagination") != null) {
          paginatedResult.pagination = JSON.parse(res.headers.get("Pagination"));
        }
        return paginatedResult;
      }))
      .catch(this.handelError);
    // return this.http.get<HttpResponse<Object>>(this.baseUrl + "api/users"+queryString, {observe: 'response'}).pipe(
    //   tap(resp => console.log('heaeder', resp.headers.get('ReturnStatus'))
    //   )
  }

  getUser(id): Observable<User> {
    return this.http.get(this.baseUrl + "api/users/" + id)
      .map(response => <User>response)
      .catch(this.handelError);
  }

  updateUser(id: number, user: User) {
    return this.http.put(this.baseUrl + "api/users/" + id, user).catch(this.handelError);
  }

  sendLike(id: number, recipientId: number) {
    return this.http.post(this.baseUrl + "api/users/" + id + "/like/" + recipientId, {}).catch(this.handelError);
  }

  getMessages(id: number, page?: number, itemsPerPage?: number, messageContainer?: string) {
    const paginatedResult: PaginatedResult<Message[]> = new PaginatedResult<Message[]>();
    let queryString = "?MessageContainer=" + messageContainer;

    if (page != null && itemsPerPage != null) {
      queryString += "&pageNumber=" + page + "&pageSize=" + itemsPerPage;
    }

    return this.http.get(this.baseUrl + "api/users/" + id + "/messages" + queryString, {observe: 'response'})
      .pipe(map((res: HttpResponse<Message[]>) => {
        paginatedResult.result = res.body;

        if (res.headers.get("Pagination") != null) {
          paginatedResult.pagination = JSON.parse(res.headers.get("Pagination"));
        }
        return paginatedResult;
      }))
      .catch(this.handelError);
  }

  getMessageThread(id: number, recipientId: number) {
    return this.http.get(this.baseUrl + "api/users/" + id + "/messages/thread/" + recipientId, {observe: 'response'})
      .pipe(map((response: HttpResponse<Message[]>) => {
        return response.body
      })).catch(this.handelError);
  }

  sendMessage(id: number, message: Message) {
    return this.http.post(this.baseUrl + "api/users/" + id + "/messages", message, {observe: "response"})
      .pipe(map((res: HttpResponse<Message>) => {
        return res.body;
      })).catch(this.handelError);
  }

  deleteMessage(id: number, userId: number) {
    return this.http.post(this.baseUrl + "api/users/" + userId + "/messages/" + id, {}).catch(this.handelError);
  }

  markAsRead(userId: number, messageId: number) {
    this.http.post(this.baseUrl + "api/users/" + userId + "/messages/" + messageId + "/read", {})
      .subscribe();
  }

  private handelError(error: HttpErrorResponse) {
    const applicationError = error.headers.get("Application-Error");
    if (applicationError) {
      return Observable.throw(applicationError);
    }
    const serverError = error.error;
    let modelStateErrors = '';
    if (serverError) {
      for (const key in serverError) {
        if (serverError[key]) {
          if (error.status == 400) {
            modelStateErrors += serverError[key] + '';
          } else {
            modelStateErrors += serverError[key] + '\n';
          }
        }
      }
    }
    return observableThrowError(
      modelStateErrors || 'Server error'
    );
  }

  setMainPhoto(userId: number, id: number) {
    return this.http.post(this.baseUrl + "api/users/" + userId + "/photos/" + id + "/setmain", {}).catch(this.handelError);
  }

  deletePhoto(userId: number, photoId: number) {
    return this.http.delete(this.baseUrl + "api/users/" + userId + "/photos/" + photoId).catch(this.handelError);
  }
}

