
import {throwError as observableThrowError, BehaviorSubject} from 'rxjs';
import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import { map } from 'rxjs/operators';
import {JwtHelperService} from "@auth0/angular-jwt";
import {User} from "../_models/user";
import {LoginModel} from "../_models/LoginModel";
import 'rxjs/add/operator/catch';


@Injectable()
export class AuthService {

  userToken: any;
  private readonly baseUrl: string;
  decodedToken:any;
  currentUser: User;
  jwtHelper: JwtHelperService = new JwtHelperService();
  private photoUrl = new BehaviorSubject<string>("../../assets/user.png");
  currentPhotoUrl = this.photoUrl.asObservable();

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  changeMemberPhoto(photoUrl: string){
    this.photoUrl.next(photoUrl);
  }

  login(user: User) {
    return this.http.post<LoginModel>(this.baseUrl + "api/auth/login", user).pipe(map((result: LoginModel) => {
      const user = result;
      if (user && user.tokenString){
        localStorage.setItem("token", result.tokenString);
        localStorage.setItem("user", JSON.stringify(user.user));
        this.decodedToken = this.jwtHelper.decodeToken(user.tokenString);
        this.currentUser = user.user;
        this.userToken = user.tokenString;
        if (this.currentUser.photoUrl != null){
          this.changeMemberPhoto(this.currentUser.photoUrl);
        }else{
          this.changeMemberPhoto("../../assets/user.png");
        }
      }
    })).catch(this.handelError);
  }

  loggedIn(){
    return !this.jwtHelper.isTokenExpired(localStorage.getItem('token'));
  }

  register(model: any) {
    return this.http.post(this.baseUrl + "api/auth/register",model).catch(this.handelError);
  }

  private handelError(error: HttpErrorResponse){
    const applicationError = error.headers.get("Application-Error");
    if (applicationError){
      return observableThrowError(applicationError);
    }

    const serverError = error.error;
    let modelStateErrors = '';
    if (serverError){
      for (const key in serverError) {
        if (serverError[key]) {
          modelStateErrors += serverError[key] + '\n';
        }
      }
    }
    return observableThrowError(
      modelStateErrors || 'Server error'
    );
  }
}
