import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {User} from "../_models/user";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {UserService} from "../_services/user.service";
import {AlertifyService} from "../_services/alertify.service";
import "rxjs/add/operator/map";

@Injectable()
export class MemberDetailResolver implements Resolve<User>{

  constructor(private userService: UserService,
              private router: Router,
              private alertify: AlertifyService){

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> | Promise<User> | User {
    return this.userService.getUser(route.params['id']).catch(err => {
      this.alertify.error("Problem retrieving data");
      this.router.navigate(['/app/members']);
      return Observable.of(null);
    })
  }
}
