﻿import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {UserService} from "../_services/user.service";
import {AlertifyService} from "../_services/alertify.service";
import "rxjs/operator/catch";
import 'rxjs/add/observable/of';
import {Message} from "../_models/message"
import {AuthService} from "../_services/auth.service";

@Injectable()
export class MessagesResolver implements Resolve<Message[]> {

  pageSize = 5;
  pageNumber = 1;
  messageContainer = "Unread";


  constructor(private userService: UserService,
              private router: Router,
              private authService: AuthService,
              private alertify: AlertifyService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Message[]> | Promise<Message[]> | Message[] {
    return this.userService.getMessages(this.authService.decodedToken.nameid, this.pageNumber, this.pageSize, this.messageContainer)
      .catch(err => {
        this.alertify.error("Problem retrieving data");
        this.router.navigate(['/home']);
        return Observable.of(null);
      });
  }
}
