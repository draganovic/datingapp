import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {User} from "../_models/user";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {UserService} from "../_services/user.service";
import {AlertifyService} from "../_services/alertify.service";
import "rxjs/operator/catch";
import 'rxjs/add/observable/of';


@Injectable()
export class MemberListResolver implements Resolve<User[]>{

  pageSize = 5;
  pageNumber = 1;

  constructor(private userService: UserService,
              private router: Router,
              private alertify: AlertifyService){

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User[]> | Promise<User[]> | User[] {
    return this.userService.getUsers(this.pageNumber, this.pageSize).catch(err => {
      this.alertify.error("Problem retrieving data");
      this.router.navigate(['/home']);
      return Observable.of(null);
    });
  }
}
