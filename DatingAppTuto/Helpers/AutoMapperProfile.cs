﻿using System.Linq;
using AutoMapper;
using DatingAppTuto.Dtos;
using DatingAppTuto.Model;

namespace DatingAppTuto.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserForListDto>()
                .ForMember(destinationMember => destinationMember.PhotoUrl,
                    options =>
                    {
                        options.MapFrom(sourceMember => sourceMember.Photos.FirstOrDefault(p => p.IsMain).Url);
                    })
                .ForMember(destinationMember => destinationMember.Age,
                    options =>
                    {
                        options.ResolveUsing(d => d.DateOfBirth.CalculateAge());
                    });
            
            CreateMap<User, UserForDetailedDto>()
                .ForMember(destinationMember => destinationMember.PhotoUrl,
                    options =>
                    {
                        options.MapFrom(sourceMember => sourceMember.Photos.FirstOrDefault(p => p.IsMain).Url);
                    })
                .ForMember(destinationMember => destinationMember.Age,
                    options =>
                    {
                        options.ResolveUsing(d => d.DateOfBirth.CalculateAge());
                    });

            CreateMap<Photo, PhotoForDetailedDto>();

            CreateMap<UserForUpdateDto, User>();

            CreateMap<PhotoForCreatingDto, Photo>();
            CreateMap<Photo, PhotoForReturnDto>();

            CreateMap<UserForRegisterDto, User>();

            CreateMap<MessageForCreatingDto,Message>().ReverseMap();
            CreateMap<Message, MessageToReturnDto>()
                .ForMember(m => m.SenderPhotoUrl, opt =>
                    opt.MapFrom(u => u.Sender.Photos.FirstOrDefault(p => p.IsMain).Url))
                .ForMember(m => m.RecipientPhotoUrl, opt =>
                    opt.MapFrom(u => u.Recipient.Photos.FirstOrDefault(p => p.IsMain).Url));
        }
    }
}